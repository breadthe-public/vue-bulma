# vue-bulma

## Project setup
```
yarn install
```

### Compiles and hot-reloads for development
```
yarn run serve
```

### Compiles and minifies for production
```
yarn run build
```

### Lints and fixes files
```
yarn run lint
```

### Run your unit tests
```
yarn run test:unit
```

### Install other dependencies
#### [Buefy](https://buefy.github.io/)
Includes [Bulma](https://bulma.io/) 0.7.1 and Buefy 0.6.6
```
yarn add buefy
```

#### [Font Awesome 4.7.0](https://fontawesome.com/v4.7.0/)
```
yarn add font-awesome
```
